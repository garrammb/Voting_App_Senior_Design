/**
 * A class used in the process of creating a user profile.
 * Methods will be provided for entering values for each variable as
 * the user registers their account. Another method will enter the corresponding
 * information in the database.
 */
package com.wordpress.speedvote.speedvote;

import java.sql.*;

public class AccountCreationBackend {
    private int studentID;
    private String username;
    private String password;
    private Sring major;
    private String email;
    private int cohort;
    private String name;

    public AccountCreationBackend(int studentID, String username, String password, String major, String email, int cohort, String name) {
        this.studentID = studentID;
        this.username = username;
        this.password = password;
        this.major = major;
        this.email = email;
        this.cohort = cohort;
        this.name = name;
    }

    public AccountCreationBackend() {
        this.studentID = null;
        this.username = null;
        this.password = null;
        this.major = null;
        this.cohort = null;
    }

    // Mutator methods
    public void setStudentID (int studentID) {this.studentID = studentID; }
    public void setUsername(String username) { this.username = username; }
    public void setPassword(String password) { this.password = password; }
    public void setMajor(String major) { this.major = major; }
    public void setCohort(int cohort) { this.cohort = cohort; }

    public void addUserToDatabase() {
        Connection conn = null;
        EncryptDecrypt encryptDecrypt = new EncryptDecrypt();
        try {
            String url = "SpeedVote"; // database url
            String databaseUser = "SpeedVote_User"; // username for database
            String databasePassword = "$p33d_Vot3!"; // password for database

            conn = DriverManager.getConnection(url,databaseUser,databasePassword);
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    System.out.println(String.format("Connected to database %s " + "successfully.", conn.getCatalog()));
                }
                String sql = "INSERT INTO user ( student_id, username, password, major, email, cohort, name ) " +
                        "values (?,?,?,?,?,?,?)";
                PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                pstmt.setInt(1, this.studentID);
                pstmt.setString(2, this.username);
                pstmt.setString(3, encryptDecrypt.encrypt(this.password));
                pstmt.setString(4, this.major);
                pstmt.setString(5, this.email);
                pstmt.setInt(6, this.cohort);
                pstmt.setString(7, this.name);

                int rowsUpdated = pstmt.executeUpdate();

                pstmt.close();
                stmt.close();
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
