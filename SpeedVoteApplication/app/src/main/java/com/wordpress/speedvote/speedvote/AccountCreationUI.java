package com.wordpress.speedvote.speedvote;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class AccountCreationUI extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_creation);
    }

    createAccount = (Button)findViewByID(R.id.accountCreationButton);
    userNameText = (EditText)findViewByID(R.id.accountCreationUsernameField).getText().toString();
    studentIDText = (EditText)findViewByID(R.id.accountCreationStudentIDField).getText().toString();
    emailText = (EditText)findViewByID(R.id.accountCreationEmailField).getText().toString();
    passwordText = (EditText)findViewByID(R.id.accountCreationPasswordField).getText().toString();
    confirmPasswordText = (EditText)findViewByID(R.id.accountCreationPasswordConfirmField).getText().toString();
    cohort = (Spinner)findViewByID(R.id.accountCreationCohortSelect).getSelectedItem().toString();


    View.OnClickListener createAccount = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(userNameText != null && studentIDText != null && emailText != null && passwordText != null && confirmPasswordText != null) {
                if (passwordText == confirmPasswordText) {
                    AccountCreationBackend accountCreationBackend = new AccountCreationBackend(studentIDText, userNameText, passwordText, major, cohort)
                    accountCreationBackend.addUserToDatabase();
                }
                else {
                    Toast.makeText(getActivity(), "Passwords do not match", Toast.LENGTH_LONG).show();
                }
            }
            else {
                Toast.makeText(getActivity(), "Please fill out all fields", Toast.LENGTH_LONG).show();
            }
        }
    }
}
