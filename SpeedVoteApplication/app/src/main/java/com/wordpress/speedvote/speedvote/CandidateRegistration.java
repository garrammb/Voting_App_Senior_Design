/**
 * A class used to register for an election as a candidate.
 * Methods will be provided to change the values for candidate name, major, student ID,
 * and cohort. It will also store whether or not the candidate has signed the petition.
 */
package com.wordpress.speedvote.speedvote;

public class CandidateRegistration {
    private String candidateName;
    private String major;
    private int studentID;
    private int cohort;
    private boolean signed;

    public CandidateRegistration(String candidateName, String major, int studentID, int cohort, boolean signed) {
        this.candidateName = candidateName;
        this.major = major;
        this.studentID = studentID;
        this.cohort = cohort;
        this.signed = signed;
    }

    public CandidateRegistration() {
        this.candidateName = null;
        this.major = null;
        this.studentID = null;
        this.cohort = null;
        this.signed = false;
    }

    // Mutator Methods
    public void setCandidateName(String candidateName) {this.candidateName = candidateName; }
    public void setMajor(String major) { this.major = major; }
    public void setStudentID(int studentID) { this.studentID = studentID; }
    public void setCohort(int cohort) { this.cohort = cohort; }
    public void setSigned(boolean signed) { this.signed = signed; }

    public void register() {
        // Submit data to database
    }

    public void bringUpConstitution() {
        // Bring up constitution page and prompt user to sign
    }
}