/**
 * A class used to register for an election as a candidate.
 * Methods will be provided to change the values for candidate name, major, student ID,
 * and cohort. It will also store whether or not the candidate has signed the petition.
 */
package com.wordpress.speedvote.speedvote;

import java.sql.*;

public class CandidateRegistrationBackend {
    private int candidateID = null;
    private String candidateName;
    private String major;
    private int studentID;
    private int cohort;
    private String position;
    private boolean signed;

    private boolean cusa;
    private boolean classOfficer;


    public CandidateRegistrationBackend(String candidateName, String major, int studentID, int cohort, String position, boolean isCUSA) {
        this.candidateName = candidateName;
        this.major = major;
        this.studentID = studentID;
        this.cohort = cohort;
        this.position = position;

        this.signed = false;

        if (isCUSA == true) { cusa = true; classOfficer = false; }
        else { cusa = false; classOfficer = true; }
    }

    public CandidateRegistrationBackend() {
        this(null, null, null, null, null, true);
        this.signed = false;
    }

    // Mutator Methods
    public void setCandidateName(String candidateName) { this.candidateName = candidateName; }
    public void setMajor(String major) { this.major = major; }
    public void setStudentID(int studentID) { this.studentID = studentID; }
    public void setCohort(int cohort) { this.cohort = cohort; }
    public void setPosition(String position) { this.position = position; }
    public void setIsCUSA(boolean isCUSA) {
        if (isCUSA == true) {
            cusa = true;
            classOfficer = false;
        }
        else {
            cusa = false;
            classOfficer = true;
        }

    }
    public void setSigned() { this.signed = true; }

    public void register() {
        // electionID thing needs to be done
        if (!isRegistered()) {
            Connection conn = null;
            try {
                String url = "SpeedVote"; // database url
                String databaseUser = "SpeedVote_User"; // username for database
                String databasePassword = "$p33d_Vot3!"; // password for database

                conn = DriverManager.getConnection(url,databaseUser,databasePassword);
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            } finally {
                try {
                    if (conn != null) {
                        System.out.println(String.format("Connected to database %s " + "successfully.", conn.getCatalog()));
                    }
                    String sql = "INSERT INTO candidate ( student_id, cohort, position, cusa, class_officer ) " +
                            "values (?,?,?,?,?,?)";
                    PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                    pstmt.setInt(1, this.studentID);
                    pstmt.setInt(2, this.cohort);
                    pstmt.setString(3, this.position);
                    pstmt.setBoolean(4, this.cusa);
                    pstmt.setBoolean(5, this.classOfficer);

                    int rowsUpdated = pstmt.executeUpdate();

                    pstmt.close();
                    stmt.close();
                    conn.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
        else {
            Toast.makeText(getActivity(), "This user is already registered for this position.");
        }
    }

    public void getCandidateID() {
        Connection conn = null;
        try {
            // db parameters
            String url = "SpeedVote";
            String user = "SpeedVote_User";
            String password = "$p33d_Vot3!";
            // create a connection to the database
            conn = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    System.out.println(String.format("Connected to database %s "
                            + "successfully.", conn.getCatalog()));
                }

                String sql = "SELECT candidate_id FROM candidate WHERE candidate_id = ( SELECT MAX(candidate_id) FROM candidate )";
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);
                this.candidateID = rs.getInt("candidate_id") + 1;

                rs.close();
                stmt.close();
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public boolean isRegistered() {
        Connection conn = null;
        try {
            // db parameters
            String url = "SpeedVote";
            String user = "SpeedVote_User";
            String password = "$p33d_Vot3!";
            // create a connection to the database
            conn = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    System.out.println(String.format("Connected to database %s "
                            + "successfully.", conn.getCatalog()));
                }

                String sql = "SELECT student_id FROM candidate WHERE student_id = " + this.studentID + " AND position = " + this.position;
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);
                int maybeStudentID = null;
                maybeStudentID = rs.getInt("student_id");
                maybePosition = rs.getString("position");


                rs.close();
                stmt.close();
                conn.close();

                if ((maybeStudentID != null) && (maybePosition != null))
                    return true;
                return false;
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }


    public void bringUpConstitution() {
        // Bring up constitution page and prompt user to sign
        Toast.makeText(getActivity(), "Constitution.");
    }
}
