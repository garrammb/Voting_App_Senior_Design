package com.wordpress.speedvote.speedvote;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class CandidateRegistrationUI extends AppCompatActivity {
    public static EditText candidateNameInput;
    public static String candidateNameVal;

    public static Spinner majorSpinner;
    public static String majorVal;

    public static Spinner cohortSpinner;
    public static int cohortVal;

    public static RadioButton classOfficer;
    public static RadioButton cusaOfficer;
    public static String classOfficerVal;
    public static String cusaOfficerVal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_registration);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Spinner majorSpinner = (Spinner) findViewById(R.id.candidateRegistrationMajorSpinner);
        ArrayAdapter<CharSequence> majorSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.majorArray, android.R.layout.simple_spinner_item);
        majorSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        majorSpinner.setAdapter(majorSpinnerAdapter);

        Spinner cohortSpinner = (Spinner) findViewById(R.id.candidateRegistrationCohortSpinner);
        ArrayAdapter<CharSequence> cohortSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.cohortArray, android.R.layout.simple_spinner_item);
        cohortSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cohortSpinner.setAdapter(cohortSpinnerAdapter);
    }

    public void buttonOnClick(View v) {
        candidateNameInput = (EditText) findViewById(R.id.candidateRegistrationNameField);
        candidateNameVal = candidateNameInput.getText().toString();

        majorSpinner = (Spinner) findViewById(R.id.candidateRegistrationMajorSpinner);
        majorVal = majorSpinner.getText().toString();

        cohortSpinner = (Spinner) findViewById(R.id.candidateRegistrationCohortSpinner);
        cohortVal = cohortSpinner.getText().toString();

        classOfficer = (RadioButton) findViewById(r.id.candidateRegistrationClassOfficerSelect);
        classOfficerVal = classOfficer.getText().toString();

        cusaOfficer = (RadioButton) findViewById(r.id.candidateRegistrationCUSAOfficerSelect);
        cusaOfficerVal = cusaOfficer.getText().toString();
    }
}
