package com.wordpress.speedvote.speedvote;

public class EncryptDecrypt {
    private static String KEY = "mgkgjcec";

    public EncryptDecrypt() {}

    public String encrypt( String inputStringToEncrypt) {
        String encryptedString = "";
        int currentKeyIndex = 0;
        for (int i = 0; i < inputStringToEncrypt.length(); i++) {
            char currentChar = inputStringToEncrypt.charAt(i);
            if (Character.isLetter(currentChar)) {
                int newChar = currentChar + KEY.charAt(currentKeyIndex);
                if ((int) KEY.charAt(currentKeyIndex) > 90)
                    newChar -= 97;
                else
                    newChar -= 65;
                if ((Character.isUpperCase(currentChar) && newChar > 90)
                        || (Character.isLowerCase(currentChar) && newChar > 122) )
                    newChar -= 26;
                currentChar = (char) newChar;

                currentKeyIndex++;
                if (currentKeyIndex >= KEY.length())
                    currentKeyIndex = 0;
            }
            encryptedString += currentChar;
        }
        return encryptedString;
    }

    public String decrypt( String inputStringToDecrypt) {
        String decryptedString = "";
        int currentKeyIndex = 0;
        for (int i = 0; i < inputStringToDecrypt.length(); i++) {
            char currentChar = inputStringToDecrypt.charAt(i);
            if (Character.isLetter(currentChar)) {
                int newChar = currentChar - KEY.charAt(currentKeyIndex);
                if ((int) KEY.charAt(currentKeyIndex) < 97)
                    newChar += 65;
                else
                    newChar += 97;
                if((Character.isUpperCase(currentChar) && newChar < 65)
                        || (Character.isLowerCase(currentChar) && newChar < 97))
                    newChar += 26;
                currentChar = (char) newChar;

                currentKeyIndex++;
                if (currentKeyIndex >= KEY.length())
                    currentKeyIndex = 0;
            }
            decryptedString += currentChar;
        }
        return decryptedString;
    }
}