package com.wordpress.speedvote.speedvote;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomePageAltUI extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page_alt);

        Button userSettings = (Button) findViewById(R.id.UserSettingsButton);
        userSettings.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Implement login credential check here
                Intent goToUserSettings = new Intent(HomePageAltUI.this,UserSettingsAltUI.class);
                startActivity(goToUserSettings);
            }
        });

        Button electionOverview = (Button) findViewById(R.id.ElectionOverviewButton);
        electionOverview.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Implement login credential check here
                Intent goToElectionOverview = new Intent(HomePageAltUI.this,ElectionOverviewUI.class);
                startActivity(goToElectionOverview);
            }
        });

        Button candidateRegistration = (Button) findViewById(R.id.CandidateRegistrationButton);
        candidateRegistration.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Implement login credential check here
                Intent goToCandidateRegistration = new Intent(HomePageAltUI.this,CandidateRegistrationUI.class);
                startActivity(goToCandidateRegistration);
            }
        });
    }
}
