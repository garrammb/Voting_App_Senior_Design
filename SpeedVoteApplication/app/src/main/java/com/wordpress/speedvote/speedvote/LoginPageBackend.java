package com.wordpress.speedvote.speedvote;

import java.sql.*;

public class LoginPageBackend {
    private String username;
    private String password;

    public LoginPageBackend(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public LoginPageBackend() {}

    public void setUsername(String username) { this.username = username; }
    public void setPassword(String password) { this.password = password; }

    public bool login() {
        if (usernameCheck() != null) {
            if (passwordCheck()) {
                return true;
            } else {
                Toast.makeText(getActivity(), "Incorrect Password.");
                return false;
            }
        }
        else {
            Toast.makeText(getActivity(), "User does not exist.");
            return false;
        }
        return false;
    }

    public String usernameCheck() {
        Connection conn = null;
        try {
            // db parameters
            String url = "SpeedVote";
            String user = "SpeedVote_User";
            String password = "$p33d_Vot3!";
            // create a connection to the database
            conn = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    System.out.println(String.format("Connected to database %s "
                            + "successfully.", conn.getCatalog()));
                }

                String sql = "SELECT password FROM user WHERE username = " + this.username;
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);
                return rs.getString("password");
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public boolean passwordCheck() {
        EncryptDecrypt encryptDecrypt = new EncryptDecrypt();
        if (encryptDecrypt.decrypt(usernameCheck()) == this.password)
            return true;
        return false;
    }