package com.wordpress.speedvote.speedvote;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LoginPageUI extends AppCompatActivity {
    public static EditText usernameInput;
    public static String usernameVal;

    public static EditText passwordInput;
    public static String passwordVal;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        TextView createAccount = (TextView) findViewById(R.id.loginPageCreateAccountText);
        createAccount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent goToCreateAccount = new Intent(LoginPageUI.this,AccountCreationUI.class);
                startActivity(goToCreateAccount);
            }
        });

        Button login = (Button) findViewById(R.id.loginPageLoginButton);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                usernameInput = (EditText) findViewById(R.id.loginPageUsernameField);
                usernameVal = usernameInput.getText().toString();

                passwordInput = (EditText) findViewById(R.id.loginPagePasswordField);
                passwordVal = passwordInput.getText().toString();
                
                // get username and password boxes and use checks for LoginPageBackend
                LoginPageBackend loginAttempt = new LoginPageBackend(usernameVal,passwordVal);
                if (loginAttempt.login()) {
                    Intent goToHomePage = new Intent(LoginPageUI.this, HomePageAltUI.class);
                    startActivity(goToHomePage);
                }
            }
        });

    }

    //public void buttonOnClick(View v) {

    //}
}
