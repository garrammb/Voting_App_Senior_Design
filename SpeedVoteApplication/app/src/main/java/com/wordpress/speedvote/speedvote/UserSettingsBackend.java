/**
 A class that will be used for functionality on the user settings screen.
 It contains get functions for each piece of necessary data as well as
 a funciton to change a user's password.
 */
package com.wordpress.speedvote.speedvote;

public class UserSettingsBackend {
    private String username;
    private String major;
    private int studentID;
    private int cohort;

    public UserSettingsBackend(String username, String major, int studentID, int cohort) {
        this.username = username;
        this.major = major;
        this.studentID = studentID;
        this.cohort = cohort;
    }

    public UserSettingsBackend() {
        this(null, null, null, null);
    }

    public String getUserName { return this.username; }
    public Stirng getMajor { return this.major; }
    public int getStudentID { return this.studentID; }
    public int getCohort { return this.cohort; }

    public void changePassword() {
        // Send email to user to enable them to change their password.
    }
}
